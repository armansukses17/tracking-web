<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crud extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->r_tracking = $this->load->database('r_db_tpsonline_tracking',true);
		$this->w_tracking = $this->load->database('w_db_tpsonline_tracking',true);
	}

	# Start Data table 2 ------------------------------------------------ // with group by
    function dataTableQuery2($db,$select,$table,$where,$column_search,$column_order,$order,$joinTable,$groupBy)
	{
		$this->$db->select($select);
		$this->$db->from($table);
        $this->$db->where($where);
        $this->$db->group_by($groupBy);

        if(count((array)$joinTable)){
            foreach($joinTable as $join){
                $this->$db->join($join['table'],$join['relation'],'INNER');
            }
        }
		$i = 0;
		foreach ($column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->$db->group_start();
					$this->$db->like($item, $_POST['search']['value']);
				}else{
					$this->$db->or_like($item, $_POST['search']['value']);
				}
				if(count($column_search) - 1 == $i) //last loop
				$this->$db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->$db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($order)){
			$this->$db->order_by(key($order), $order[key($order)]);
		}
	}

	function getDatatable2($db,$select,$table,$where,$column_search,$column_order,$order,$joinTable,$groupBy)
	{
		$this->dataTableQuery2($db,$select,$table,$where,$column_search,$column_order,$order,$joinTable,$groupBy);
		if($_POST['length'] != -1)
		$this->$db->limit($_POST['length'], $_POST['start']);
		$query = $this->$db->get();
		return $query->result();
	}

	function dataTableCount2($db,$table,$where,$joinTable,$groupBy)
	{
        $this->$db->from($table);
        if(count((array)$joinTable)){
            foreach($joinTable as $join){
                $this->$db->join($join['table'],$join['relation'],'INNER');
            }
        }
		$this->$db->where($where);
		$this->$db->group_by($groupBy);
		return $this->$db->count_all_results();
	}
	
	function dataTableFilter2($db,$select,$table,$where,$column_search,$column_order,$order,$joinTable,$groupBy)
	{
		$this->dataTableQuery2($db,$select,$table,$where,$column_search,$column_order,$order,$joinTable,$groupBy);
		$query = $this->$db->get();
		return $query->num_rows();
	}
	# End Data table 2 ------------------------------------------------
}
