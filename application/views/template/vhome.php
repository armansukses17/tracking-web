<?=$header;?>
<?=$navbar;?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
    <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
        <?=$sidebar;?>
    </div>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <?php //echo $breadcrumb; ?>
        <div class="m-content" style="margin-top: 30px; min-height: 400px;">
            <?=$content?>
        </div>
    </div>
</div>
<?=$footer;?>
<?=$ajax;?>