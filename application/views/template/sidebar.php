<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
	<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow " style="padding-top: 15px;">
		<li class="m-menu__section " style="margin-top: 0;">
			<h4 class="m-menu__section-text">NAVIGATION</h4>
			<i class="m-menu__section-icon flaticon-more-v2"></i>
		</li>
		<li class="m-menu__item" aria-haspopup="true">
			<a href="<?= base_url('./')?>" class="m-menu__link ">
				<i class="m-menu__link-icon flaticon-share"></i>
				<span class="m-menu__link-title"> 
					<span class="m-menu__link-wrap"> 
						<span class="m-menu__link-text">Tracking</span>
					</span>
				</span>
			</a>
		</li>
		<!-- <li class="m-menu__item" aria-haspopup="true">
			<a href="#" class="m-menu__link ">
				<i class="m-menu__link-icon flaticon-signs-1"></i>
				<span class="m-menu__link-title"> 
					<span class="m-menu__link-wrap"> 
						<span class="m-menu__link-text">Edit</span>
					</span>
				</span>
			</a>
		</li>
		<li class="m-menu__item" aria-haspopup="true">
			<a href="#" class="m-menu__link ">
				<i class="m-menu__link-icon flaticon-signs-1"></i>
				<span class="m-menu__link-title"> 
					<span class="m-menu__link-wrap"> 
						<span class="m-menu__link-text">View</span>
					</span>
				</span>
			</a>
		</li>
		<li class="m-menu__item" aria-haspopup="true">
			<a href="#" class="m-menu__link ">
				<i class="m-menu__link-icon flaticon-signs-1"></i>
				<span class="m-menu__link-title"> 
					<span class="m-menu__link-wrap"> 
						<span class="m-menu__link-text">Help</span>
					</span>
				</span>
			</a>
		</li> -->
	</ul>
</div>