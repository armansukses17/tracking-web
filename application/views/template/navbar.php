<section id="subheader" class="page-track no-bottom" data-stellar-background-ratio="0.5" style="position: relative;">
    <div class="overlay parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url('assets/img/map.png'); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-custom">
                    	<span class="my-breadcrumb">
                    		<?=$title?>
                    		<span class="line"></span>
                    	</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
// var prevScrollpos = window.pageYOffset;
// window.onscroll = function() {
// 	var currentScrollPos = window.pageYOffset;
// 	if (prevScrollpos > 300) { // > currentScrollPos
// 		$('#hrd-desktop').css({"background-color": "#2c2e3e"});
// 	} else {
// 		$('#hrd-desktop').css({"background-color": "transparent"});
// 	}
// 	prevScrollpos = currentScrollPos;
// } 
</script>
<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
	<div class="m-container m-container--fluid m-container--full-height">
		<div id="hrd-desktop" class="m-stack m-stack--ver m-stack--desktop">
			<?php  
			$activeDomain = $this->config->item('prefixDomain');
			if ($activeDomain == 'MAU')
			{
			    $logo = 'assets/img/logo-mau.png';
			    $width = '45';
			    $addTextLogo = '<div style="color: #9d9caf; position: relative; left: 50px; top: -10px; font-size: 10px; font-style: italic;">TRACK & TRACE</div>';

			}
			elseif ($activeDomain == 'RA')
			{
			    $logo = 'assets/img/logo-ra.png';
			    $width = '30';
			    $addTextLogo = '';
			}
			elseif ($activeDomain == 'LOCAL') // LOCAL DEFAULT PAKE SALAH SATU
			{
			    $logo = 'assets/img/logo-mau.png';
			    $width = '45';
			    $addTextLogo = '<div style="color: #9d9caf; position: relative; left: 45px; top: -10px; font-size: 10px; font-style: italic;">TRACK & TRACE</div>';

			    // $logo = 'assets/img/logo-ra.png';
			    // $width = '30';
			    // $addTextLogo = '';
			}
			?>
			<!-- BEGIN: Brand -->
			<div class="m-stack__item m-brand  m-brand--skin-dark ">
				<div class="m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-brand__logo" style="width: 150px; font-size: 16px;">
						<a href="<?= base_url();?>" class="m-brand__logo-wrapper" style="color: #fff; text-decoration: auto; font-weight: bold;">
							<img alt="" src="<?= base_url() . $logo; ?>" height="<?= $width; ?>"/>
							<!-- <i class="fa fa-map-marker" aria-hidden="true"></i> TRACK & TRACE -->
						</a>
						<?= $addTextLogo; ?>
					</div>
					<div class="m-stack__item m-stack__item--middle m-brand__tools">

						<!-- BEGIN: Left Aside Minimize Toggle -->
						<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
							<span></span>
						</a>

						<!-- END -->

						<!-- BEGIN: Responsive Aside Left Menu Toggler -->
						<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<!-- END -->
					</div>
				</div>
			</div>

			<!-- END: Brand -->
			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
				<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
					<div class="m-stack__item m-topbar__nav-wrapper">
					</div>
				</div>
				<!-- END: Topbar -->
			</div>
		</div>
	</div>
</header>