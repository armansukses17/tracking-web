<?php  
$activeDomain = $this->config->item('prefixDomain');
if ($activeDomain == 'MAU')
{
    $getTrackingFirstLoad = 'tracking/getTrackingFirstLoadMau';
    $getTrackingStatus = 'tracking/getTrackingStatusMau';
    $formID = 'formTrackingSrcMau';
}
elseif ($activeDomain == 'RA')
{
    $getTrackingFirstLoad = 'tracking/getTrackingFirstLoadRa';
    $getTrackingStatus = 'tracking/getTrackingStatusRa';
    $formID = 'formTrackingSrcRa';
}
elseif ($activeDomain == 'LOCAL') // LOCAL DEFAULT PAKE SALAH SATU
{
    $getTrackingFirstLoad = 'tracking/getTrackingFirstLoadMau';
    $getTrackingStatus = 'tracking/getTrackingStatusMau';
    $formID = 'formTrackingSrcMau';
    // --------------------------------------------------------
    // $getTrackingFirstLoad = 'tracking/getTrackingFirstLoadRa';
    // $getTrackingStatus = 'tracking/getTrackingStatusRa';
    // $formID = 'formTrackingSrcRa';
}
?>
<div class="row">
    <div class="col-md-12 mb-5">
        <form id="<?php echo $formID; ?>">
            <div class="input-group input-group-lg m-input-group" style="position: relative;">
                <img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking">
                <input type="text" name="trackingNo" id="trackingNo" class="form-control" placeholder="Insert max 10 tracking number, separate with commas ..." autocomplete="off">
                <div class="input-group-append">
                    <button class="btn btn-danger" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <div class="m-portlet m-portlet--full-height " id="resultData" style="display: none;">
            <div class="m-portlet__head" id="resultStatus">
                <div class="m-portlet__head-caption" style="width: 100px;">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Result : 
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools" style="width: 100%;">                    
                    <ul id="resultTrackNo" class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                        
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-12" id="loaderTracking2">
                        <img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="">
                    </div>
                     <div class="col-md-12 mb-5" id="info-shipper-consignee">
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-panex" id="xxxzzz">
                        <!--Begin::Timeline 3 -->
                        <div class="m-timeline-3">
                            <div class="m-timeline-3__items" id="listByTrackNo">
                               
                            </div>
                        </div>
                        <!--End::Timeline 3 -->
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Audit Log-->
    </div>
</div>

<style>
    .m-portlet .m-portlet__head {
        padding-top: 30px;
        padding-bottom: 30px;
        height: auto;
        background: #e4ebfa;
        border-bottom: 2px solid #d5ddef;
    }
    .m-timeline-3 .m-timeline-3__item {
        margin-bottom: 22px;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc {
        padding-left: 30px;
    }
    .m-timeline-3 .m-timeline-3__item:before {
        top: 5px;
        left: 0;
        width: 14px;
        border-radius: 100%;
        height: 14px;
    }
    .m-timeline-3 .m-timeline-3__item:after {
        content: "";
        position: absolute;
        top: 23px;
        left: 7px;
        height: 100%;
        border-left: 1px solid #dadbe7;
        z-index: 0;
    }
    .nav.nav-pills.m-nav-pills--btn-sm .m-tabs__link {
        border: 1px solid #859bc8;
        background: #ffffff;
        color: #859bc8;
    }
    .nav.nav-pills.nav-pills--brand .nav-link.active {
        background: #1fd2c2;
        color: #fff;
        border-color: #1ca99d;
    }
    .nav.nav-pills .nav-item, .nav.nav-tabs .nav-item {
        margin-bottom: 5px;
    }
    #info-shipper-consignee {
        font-size: 17px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #info-shipper-consignee .trackInfoHdrTitle {
        /*font-weight: normal;*/
    }
    #info-shipper-consignee .trackInfoHdr {
        margin-left: 5px;
        font-weight: normal;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc .m-timeline-3__item-text {
        font-size: 16px;
    }
    .m-timeline-3 .m-timeline-3__item .m-timeline-3__item-desc .m-timeline-3__item-user-name .m-timeline-3__item-link {
        font-size: 13px;
        text-decoration: none;
        color: #d06c6c;
    }
    #loaderTracking {
        display: none;
        width: 20px;
        position: absolute;
        right: 137px;
        top: 20px;
        bottom: 0;
        z-index: 999;
    }
    #loaderTracking2 {
        display: none;
        width: 30px;
        z-index: 20;
    }
    #trackingNo {
        font-size: 14px;
        /*border: 2px solid #e5e5e5 !important;*/
        border-color: transparent !important;
    }
    #formTrackingSrcMau,
    #formTrackingSrcRa {
        border: 15px solid #e8e8ec;
    }
</style>

<script>
function clearData() {
    $('#info-shipper-consignee').empty();
    $('#listByTrackNo').empty(); // netral
}

$('#formTrackingSrcMau').on('submit',function(e) {
    e.preventDefault();
    clearData();
    $('html, body').animate({
        scrollTop: $("#resultStatus").offset().top
    }, 1000);

    var trackingNo = $('#trackingNo').val();
    $.ajax({
        url: '<?php echo base_url() . $getTrackingFirstLoad; ?>',
        type: "post",
        data: {trackingNo : trackingNo},
        dataType: "json",
        beforeSend: function(jqXHR, options) {
            setTimeout(function() {
                $.ajax($.extend(options, {beforeSend: $.noop}));
            }, 500);
            $('#loaderTracking').show();
            return false;
        },
        success: function(data) {
            $('#loaderTracking').hide();
            if (data.s == 'success') {
                $('#resultData').show();
                var s = '';
                var active = '';
                var num = 1;
                var noo = '';
                var noTrack = 'noTrack';
                // --------------------------------------------
                for (var i = 0; i < data.dataKey.length; i++) {  
                    noo = num++;
                    // noTrack-1-AEI03000001189   -> no 1 is active
                    if (noTrack+'-'+noo+'-'+data.dataKey[0] == noTrack+'-1-'+data.dataKey[i]) {
                        active = 'active show';
                    } else {
                        active = '';
                    }
                    s += `<li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link noTrack-${noo}-${data.dataKey[i]} ${active}" data-toggle="tab" href="#m_widget${noo}_tab${noo}_content" role="tab" aria-selected="true" title="${data.dataKey[i]}" onclick="getDataByTrackingNoMau(this.title)">
                                    ${data.dataKey[i]}
                                </a>
                            </li>`;
                }  
                $('#resultTrackNo').html(s);
                
                if (data.firstLoad.type == 1 || data.firstLoad.type == 2 || data.firstLoad.type == 3) {
                    for (var n = 0; n < data.firstLoad.firstData.length; n++) {  
                        if (data.firstLoad.firstData[n] != null) {
                            var infoHdr = `<div class="row">
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">SHIPPER</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].shipper_name}</span><br>
                                                    <span class="trackInfoHdrTitle">CONSIGNEE</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].consignee_name}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">FLIGHT NO</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].flight_no}</span><br>
                                                    <span class="trackInfoHdrTitle">ROUTE</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].route}</span>
                                                </div>
                                            </div>`;
                            $('#info-shipper-consignee').html(infoHdr);
                            // --------------------------------------------
                            var content =  `<div class="m-timeline-3__item m-timeline-3__item--${data.firstLoad.firstData[n].classStatusDiv}">
                                                <div class="m-timeline-3__item-desc">
                                                    <span class="m-timeline-3__item-text" id="statusInfo">
                                                    	${data.firstLoad.firstData[n].statusName}
                                                    </span>
                                                    <br />
                                                    <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link" id="statusTime">
                                                            ${data.firstLoad.firstData[n].status_date} ${data.firstLoad.firstData[n].status_time}
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>`;
                            $('#listByTrackNo').append(content);
                        } 
                    }
                }
                else {
                    $('#listByTrackNo').html('<div>Data not found !</div>');
                }
            } 
            else if (data.s == 'failMax' || data.s == 'empty') {
                    swal('Error !', data.m, 'error'); 
                    clearData();
                    $('#resultData').hide();
                }
            else {
                swal('Error !', data.m, 'error'); 
                clearData();
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
            clearData();
        }
    });
});

$('#formTrackingSrcRa').on('submit',function(e) {
    e.preventDefault();
    clearData();
    $('html, body').animate({
        scrollTop: $("#resultStatus").offset().top
    }, 1000);

    var trackingNo = $('#trackingNo').val();
    $.ajax({
        url: '<?php echo base_url() . $getTrackingFirstLoad; ?>',
        type: "post",
        data: {trackingNo : trackingNo},
        dataType: "json",
        beforeSend: function(jqXHR, options) {
            setTimeout(function() {
                $.ajax($.extend(options, {beforeSend: $.noop}));
            }, 500);
            $('#loaderTracking').show();
            return false;
        },
        success: function(data) {
            $('#loaderTracking').hide();
            if (data.s == 'success') {
                $('#resultData').show();
                var s = '';
                var active = '';
                var num = 1;
                var noo = '';
                var noTrack = 'noTrack';
                // --------------------------------------------
                for (var i = 0; i < data.dataKey.length; i++) {  
                    noo = num++;
                    // noTrack-1-AEI03000001189   -> no 1 is active
                    if (noTrack+'-'+noo+'-'+data.dataKey[0] == noTrack+'-1-'+data.dataKey[i]) {
                        active = 'active show';
                    } else {
                        active = '';
                    }
                    s += `<li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link noTrack-${noo}-${data.dataKey[i]} ${active}" data-toggle="tab" href="#m_widget${noo}_tab${noo}_content" role="tab" aria-selected="true" title="${data.dataKey[i]}" onclick="getDataByTrackingNoRa(this.title)">
                                    ${data.dataKey[i]}
                                </a>
                            </li>`;
                }  
                $('#resultTrackNo').html(s);
                
                if (data.firstLoad.type == 1) {
                    var textTracking = '';
                    var classTrack = '';
                    for (var n = 0; n < data.firstLoad.firstData.length; n++) {  
                        if (data.firstLoad.firstData[n] != null) {
                            var infoHdr = `<div class="row">
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">SHIPPER</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].shipper_name}</span><br>
                                                    <span class="trackInfoHdrTitle">CONSIGNEE</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].consignee_name}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">FLIGHT NO</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].flight_no}</span><br>
                                                    <span class="trackInfoHdrTitle">ROUTE</span> : <span class="trackInfoHdr">${data.firstLoad.firstData[n].route}</span>
                                                </div>
                                            </div>`;
                            $('#info-shipper-consignee').html(infoHdr);
                            // --------------------------------------------
                            var content =  `<div class="m-timeline-3__item m-timeline-3__item--${data.firstLoad.firstData[n].classStatusDiv}">
                                                <div class="m-timeline-3__item-desc">
                                                    <span class="m-timeline-3__item-text" id="statusInfo">
                                                       ${data.firstLoad.firstData[n].statusName}
                                                    </span>
                                                    <br />
                                                    <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link" id="statusTime">
                                                            ${data.firstLoad.firstData[n].status_date} ${data.firstLoad.firstData[n].status_time}
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>`;
                            $('#listByTrackNo').append(content);
                        } 
                    }
                }
                else {
                    $('#listByTrackNo').html('<div>Data not found !</div>');
                }
            } 
            else if (data.s == 'failMax' || data.s == 'empty') {
                    swal('Error !', data.m, 'error'); 
                    clearData();
                    $('#resultData').hide();
                }
            else {
                swal('Error !', data.m, 'error'); 
                clearData();
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
            clearData();
        }
    });
});

function getDataByTrackingNoMau(no) {
    clearData();
    $('html, body').animate({
        scrollTop: $("#resultStatus").offset().top
    }, 1000);

    $.ajax({
        url: '<?php echo base_url() . $getTrackingStatus; ?>/'+no,
        type: "post",
        data: {trackingNo : no},
        dataType: "json",
        beforeSend: function(jqXHR, options) {
            setTimeout(function() {
                $.ajax($.extend(options, {beforeSend: $.noop}));
            }, 500);
            $('#loaderTracking2').show();
            return false;
        },
        success: function(data) {
            $('#loaderTracking2').hide();
            if (data.s == 'success') {
                if (data.type == 1 || data.type == 2 || data.type == 3) {
                    var textTracking = '';
                    var classTrack = '';
                    for (var n = 0; n < data.data.length; n++) {  
                        if (data.data[n] != null) {
                            var infoHdr = `<div class="row">
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">SHIPPER</span> : <span class="trackInfoHdr">${data.data[n].shipper_name}</span><br>
                                                    <span class="trackInfoHdrTitle">CONSIGNEE</span> : <span class="trackInfoHdr">${data.data[n].consignee_name}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">FLIGHT NO</span> : <span class="trackInfoHdr">${data.data[n].flight_no}</span><br>
                                                    <span class="trackInfoHdrTitle">ROUTE</span> : <span class="trackInfoHdr">${data.data[n].route}</span>
                                                </div>
                                            </div>`;
                            $('#info-shipper-consignee').html(infoHdr);
                            // --------------------------------------------
                            var content =  `<div class="m-timeline-3__item m-timeline-3__item--${data.data[n].classStatusDiv}">
                                                <div class="m-timeline-3__item-desc">
                                                    <span class="m-timeline-3__item-text" id="statusInfo">
                                                        ${data.data[n].statusName}
                                                    </span>
                                                    <br />
                                                    <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link" id="statusTime">
                                                            ${data.data[n].status_date} ${data.data[n].status_time}
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>`;
                            $('#listByTrackNo').append(content);
                        } 
                    }
                }
                else {
                    $('#listByTrackNo').html('<div>Data not found !</div>');
                }
            }
            else {
                $('#info-shipper-consignee').html('');
                $('#listByTrackNo').html('<div>'+data.m+'</div>');
                // clearData();
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
            $('#loaderTracking2').hide();
            clearData();
        }
    });
}

function getDataByTrackingNoRa(no) {
    clearData();
    $('html, body').animate({
        scrollTop: $("#resultStatus").offset().top
    }, 1000);
    
    $.ajax({
        url: '<?php echo base_url() . $getTrackingStatus; ?>/'+no,
        type: "post",
        data: {trackingNo : no},
        dataType: "json",
        beforeSend: function(jqXHR, options) {
            setTimeout(function() {
                $.ajax($.extend(options, {beforeSend: $.noop}));
            }, 500);
            $('#loaderTracking2').show();
            return false;
        },
        success: function(data) {
            $('#loaderTracking2').hide();
            if (data.s == 'success') {
                if (data.type == 1) {
                    var textTracking = '';
                    var classTrack = '';
                    for (var n = 0; n < data.data.length; n++) {  
                        if (data.data[n] != null) {
                            var infoHdr = `<div class="row">
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">SHIPPER</span> : <span class="trackInfoHdr">${data.data[n].shipper_name}</span><br>
                                                    <span class="trackInfoHdrTitle">CONSIGNEE</span> : <span class="trackInfoHdr">${data.data[n].consignee_name}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="trackInfoHdrTitle">FLIGHT NO</span> : <span class="trackInfoHdr">${data.data[n].flight_no}</span><br>
                                                    <span class="trackInfoHdrTitle">ROUTE</span> : <span class="trackInfoHdr">${data.data[n].route}</span>
                                                </div>
                                            </div>`;
                            $('#info-shipper-consignee').html(infoHdr);
                            // --------------------------------------------
                            var content =  `<div class="m-timeline-3__item m-timeline-3__item--${data.data[n].classStatusDiv}">
                                                <div class="m-timeline-3__item-desc">
                                                    <span class="m-timeline-3__item-text" id="statusInfo">
                                                        ${data.data[n].statusName}
                                                    </span>
                                                    <br />
                                                    <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link" id="statusTime">
                                                            ${data.data[n].status_date} ${data.data[n].status_time}
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>`;
                            $('#listByTrackNo').append(content);
                        } 
                    }
                }
                else {
                    $('#listByTrackNo').html('<div>Data not found !</div>');
                }
            }
            else {
                $('#info-shipper-consignee').html('');
                $('#listByTrackNo').html('<div>'+data.m+'</div>');
                // clearData();
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
            $('#loaderTracking2').hide();
            clearData();
        }
    });
}
</script>