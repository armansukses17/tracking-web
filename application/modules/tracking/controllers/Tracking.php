<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// -------------------------------------------------------------
// TRACKING MAU & RA
// -------------------------------------------------------------
class Tracking extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
	}
	
	public function index()
	{
		$data = [
			'title' => 'Track & Trace'
		];
		$this->home_library->main('tracking/tracking',$data);
	}

	public function statusInboundMau($mawb)
	{
		// td_inbound_delivery_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query1 = $this->db->get('td_inbound_delivery_aircarft a');
		$check1 = $query1->num_rows();
		$statusName = ['statusName' => 'Delivery from aircraft to incoming warehouse', 'classStatusDiv' => 'danger'];
		$check1 > 0 ? $dataArr1 = array_merge($query1->row_array(), $statusName) : $dataArr1 = null;

		// td_inbound_breakdown
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query2 = $this->db->get('td_inbound_breakdown a');
		$check2 = $query2->num_rows();
		$statusName = ['statusName' => 'Arrival at incoming warehouse', 'classStatusDiv' => 'warning'];
		$check2 > 0 ? $dataArr2 = array_merge($query2->row_array(), $statusName) : $dataArr2 = null;

		// td_inbound_storage
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query3 = $this->db->get('td_inbound_storage a');
		$check3 = $query3->num_rows();
		$statusName = ['statusName' => 'Storage', 'classStatusDiv' => 'success'];
		$check3 > 0 ? $dataArr3 = array_merge($query3->row_array(), $statusName) : $dataArr3 = null;

		// td_inbound_clearance
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query4 = $this->db->get('td_inbound_clearance a');
		$check4 = $query4->num_rows();
		$statusName = ['statusName' => 'Custom & quarantine Clearance', 'classStatusDiv' => 'info'];
		$check4 > 0 ? $dataArr4 = array_merge($query4->row_array(), $statusName) : $dataArr4 = null;

		// td_inbound_pod
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query5 = $this->db->get('td_inbound_pod a');
		$check5 = $query5->num_rows();
		$statusName = ['statusName' => 'Received by consignee', 'classStatusDiv' => 'danger'];
		$check5 > 0 ? $dataArr5 = array_merge($query5->row_array(), $statusName) : $dataArr5 = null;

		// ------------------------------------------------------------------
		$dataArr = [$dataArr1, $dataArr2, $dataArr3, $dataArr4, $dataArr5];
		return $dataArr;
	}

	public function statusInboundTransitMau($mawb)
	{
		// td_inbound_delivery_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query1 = $this->db->get('td_inbound_delivery_aircarft a');
		$check1 = $query1->num_rows();
		$statusName = ['statusName' => 'Delivery from aircraft to incoming warehouse', 'classStatusDiv' => 'danger'];
		$check1 > 0 ? $dataArr1 = array_merge($query1->row_array(), $statusName) : $dataArr1 = null;

		// td_inbound_breakdown
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query2 = $this->db->get('td_inbound_breakdown a');
		$check2 = $query2->num_rows();
		$statusName = ['statusName' => 'Arrival at incoming warehouse', 'classStatusDiv' => 'warning'];
		$check2 > 0 ? $dataArr2 = array_merge($query2->row_array(), $statusName) : $dataArr2 = null;

		// td_inbound_storage
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query3 = $this->db->get('td_inbound_storage a');
		$check3 = $query3->num_rows();
		$statusName = ['statusName' => 'Storage', 'classStatusDiv' => 'success'];
		$check3 > 0 ? $dataArr3 = array_merge($query3->row_array(), $statusName) : $dataArr3 = null;

		// td_inbound_transit
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query4 = $this->db->get('td_inbound_transit a');
		$check4 = $query4->num_rows();
		$statusName = ['statusName' => 'Manifesting', 'classStatusDiv' => 'info'];
		$check4 > 0 ? $dataArr4 = array_merge($query4->row_array(), $statusName) : $dataArr4 = null;

		// td_inbound_transit_buildup
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query5 = $this->db->get('td_inbound_transit_buildup a');
		$check5 = $query5->num_rows();
		$statusName = ['statusName' => 'Build Up process', 'classStatusDiv' => 'danger'];
		$check5 > 0 ? $dataArr5 = array_merge($query5->row_array(), $statusName) : $dataArr5 = null;

		// td_inbound_transit_delivery_staging
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query6 = $this->db->get('td_inbound_transit_delivery_staging a');
		$check6 = $query6->num_rows();
		$statusName = ['statusName' => 'Delivery to staging area', 'classStatusDiv' => 'warning'];
		$check6 > 0 ? $dataArr6 = array_merge($query6->row_array(), $statusName) : $dataArr6 = null;

		// td_inbound_delivery_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query7 = $this->db->get('td_inbound_delivery_aircarft a');
		$check7 = $query7->num_rows();
		$statusName = ['statusName' => 'Delivery to aircraft', 'classStatusDiv' => 'success'];
		$check7 > 0 ? $dataArr7 = array_merge($query7->row_array(), $statusName) : $dataArr7 = null;

		// td_inbound_loading_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query8 = $this->db->get('td_inbound_loading_aircarft a');
		$check8 = $query8->num_rows();
		$statusName = ['statusName' => 'Loading to aircraft', 'classStatusDiv' => 'info'];
		$check8 > 0 ? $dataArr8 = array_merge($query8->row_array(), $statusName) : $dataArr8 = null;

		// ------------------------------------------------------------------
		$dataArr = [$dataArr1, $dataArr2, $dataArr3, $dataArr4, $dataArr5, $dataArr6, $dataArr7, $dataArr8];
		return $dataArr;
	}

	public function statusOutbondMau($mawb)
	{
		// td_outbond_acceptance
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query1 = $this->db->get('td_outbond_acceptance a');
		$check1 = $query1->num_rows();
		$statusName = ['statusName' => 'Acceptance confirmation', 'classStatusDiv' => 'danger'];
		$check1 > 0 ? $dataArr1 = array_merge($query1->row_array(), $statusName) : $dataArr1 = null;

		// td_outbond_weighing
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query2 = $this->db->get('td_outbond_weighing a');
		$check2 = $query2->num_rows();
		$statusName = ['statusName' => 'Weighing scale', 'classStatusDiv' => 'warning'];
		$check2 > 0 ? $dataArr2 = array_merge($query2->row_array(), $statusName) : $dataArr2 = null;

		// td_outbond_manifest
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query3 = $this->db->get('td_outbond_manifest a');
		$check3 = $query3->num_rows();
		$statusName = ['statusName' => 'Manifesting', 'classStatusDiv' => 'success'];
		$check3 > 0 ? $dataArr3 = array_merge($query3->row_array(), $statusName) : $dataArr3 = null;

		// td_outbond_storage
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query4 = $this->db->get('td_outbond_storage a');
		$check4 = $query4->num_rows();
		$statusName = ['statusName' => 'Storage position', 'classStatusDiv' => 'info'];
		$check4 > 0 ? $dataArr4 = array_merge($query4->row_array(), $statusName) : $dataArr4 = null;

		// td_outbond_buildup
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query5 = $this->db->get('td_outbond_buildup a');
		$check5 = $query5->num_rows();
		$statusName = ['statusName' => 'Build Up process', 'classStatusDiv' => 'danger'];
		$check5 > 0 ? $dataArr5 = array_merge($query5->row_array(), $statusName) : $dataArr5 = null;

		// td_outbond_delivery_staging
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query6 = $this->db->get('td_outbond_delivery_staging a');
		$check6 = $query6->num_rows();
		$statusName = ['statusName' => 'Delivery to staging area', 'classStatusDiv' => 'warning'];
		$check6 > 0 ? $dataArr6 = array_merge($query6->row_array(), $statusName) : $dataArr6 = null;

		// td_outbond_delivery_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query7 = $this->db->get('td_outbond_delivery_aircarft a');
		$check7 = $query7->num_rows();
		$statusName = ['statusName' => 'Delivery to aircraft', 'classStatusDiv' => 'success'];
		$check7 > 0 ? $dataArr7 = array_merge($query7->row_array(), $statusName) : $dataArr7 = null;

		// td_outbond_loading_aircarft
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query8 = $this->db->get('td_outbond_loading_aircarft a');
		$check8 = $query8->num_rows();
		$statusName = ['statusName' => 'Loading to aircraft', 'classStatusDiv' => 'info'];
		$check8 > 0 ? $dataArr8 = array_merge($query8->row_array(), $statusName) : $dataArr8 = null;

		// ------------------------------------------------------------------
		$dataArr = [$dataArr1, $dataArr2, $dataArr3, $dataArr4, $dataArr5, $dataArr6, $dataArr7, $dataArr8];
		return $dataArr;
	}

	public function statusRegulated($mawb)
	{
		// td_regulated_acceptance
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query1 = $this->db->get('td_regulated_acceptance a');
		$check1 = $query1->num_rows();
		$statusName = ['statusName' => 'Acceptance confirmation (before security screening)', 'classStatusDiv' => 'danger'];
		$check1 > 0 ? $dataArr1 = array_merge($query1->row_array(), $statusName) : $dataArr1 = null;

		// td_regulated_screening
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query2 = $this->db->get('td_regulated_screening a');
		$check2 = $query2->num_rows();
		$statusName = ['statusName' => 'Security screening process', 'classStatusDiv' => 'warning'];
		$check2 > 0 ? $dataArr2 = array_merge($query2->row_array(), $statusName) : $dataArr2 = null;

		// td_regulated_weighing
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query3 = $this->db->get('td_regulated_weighing a');
		$check3 = $query3->num_rows();
		$statusName = ['statusName' => 'Digital weighing scale', 'classStatusDiv' => 'success'];
		$check3 > 0 ? $dataArr3 = array_merge($query3->row_array(), $statusName) : $dataArr3 = null;

		// td_regulated_rejected
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query4 = $this->db->get('td_regulated_rejected a');
		$check4 = $query4->num_rows();
		$statusName = ['statusName' => 'Updated rejected item', 'classStatusDiv' => 'info'];
		$check4 > 0 ? $dataArr4 = array_merge($query4->row_array(), $statusName) : $dataArr4 = null;

		// td_regulated_storage
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query5 = $this->db->get('td_regulated_storage a');
		$check5 = $query5->num_rows();
		$statusName = ['statusName' => 'RA storage position', 'classStatusDiv' => 'danger'];
		$check5 > 0 ? $dataArr5 = array_merge($query5->row_array(), $statusName) : $dataArr5 = null;

		// td_regulated_csd
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query6 = $this->db->get('td_regulated_csd a');
		$check6 = $query6->num_rows();
		$statusName = ['statusName' => 'Consignment security declaration (CSD) issuance', 'classStatusDiv' => 'warning'];
		$check6 > 0 ? $dataArr6 = array_merge($query6->row_array(), $statusName) : $dataArr6 = null;

		// td_regulated_loading
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query7 = $this->db->get('td_regulated_loading a');
		$check7 = $query7->num_rows();
		$statusName = ['statusName' => 'Loading process to vehicle', 'classStatusDiv' => 'success'];
		$check7 > 0 ? $dataArr7 = array_merge($query7->row_array(), $statusName) : $dataArr7 = null;

		// td_regulated_transport
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query8 = $this->db->get('td_regulated_transport a');
		$check8 = $query8->num_rows();
		$statusName = ['statusName' => 'Transportation to warehouse lini-1', 'classStatusDiv' => 'info'];
		$check8 > 0 ? $dataArr8 = array_merge($query8->row_array(), $statusName) : $dataArr8 = null;

		// td_regulated_handover
		$this->db->select('a.id_header, a.status_date, a.status_time, b.waybill_smu, b.shipper_name, b.consignee_name, b.flight_no, concat(b.origin, " - ", b.dest) as route');
		$this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
		$this->db->where(['b.waybill_smu' => $mawb, 'a._is_active' => 1]);
		$this->db->order_by('a._created_at', 'DESC');
		$query9 = $this->db->get('td_regulated_handover a');
		$check9 = $query9->num_rows();
		$statusName = ['statusName' => 'Hand-over to airline', 'classStatusDiv' => 'danger'];
		$check9 > 0 ? $dataArr9 = array_merge($query9->row_array(), $statusName) : $dataArr9 = null;

		// ------------------------------------------------------------------
		$dataArr = [$dataArr1, $dataArr2, $dataArr3, $dataArr4, $dataArr5, $dataArr6, $dataArr7, $dataArr8, $dataArr9];
		return $dataArr;
	}

	// ===========================================================================================

	public function getTrackingFirstLoadMau()
	{
		$trackingNo = $this->input->post('trackingNo');
		$trackingNo = str_replace(array("'", '"', '/', "`", '=', '*', '^', '?'), '', $trackingNo);
		if (empty($trackingNo))
		{
			$returnAll = ['s' => 'empty', 'm' => 'TrackingNo cannot be empty !'];
		}
		else
		{
			$trackingNo = str_replace(' ', '', $trackingNo); 
			$dataTrackingNo = explode(',', $trackingNo);
			$dataTrackingNo = array_filter($dataTrackingNo); // delete is empty array
			$firstNo = $dataTrackingNo[0];

			$this->db->select('id_, gate_type');
			$this->db->where(['waybill_smu' => $firstNo, '_is_active' => 1]);
			$getHeaderInbound = $this->db->get('th_inbound');
			$checkHeaderInbound = $getHeaderInbound->num_rows();
			$HeaderInbound = $getHeaderInbound->row_array();

			if ($checkHeaderInbound > 0)
			{
				if ($HeaderInbound['gate_type'] == 'import' || $HeaderInbound['gate_type'] == 'incoming')
				{
					// ------------------------------------------
					// INBOUND BAGIAN AWAL
					// ------------------------------------------
					$dataArr = $this->statusInboundMau($firstNo);
					$return = ['type' => 1, 'firstData' => $dataArr];
				}
				elseif ($HeaderInbound['gate_type'] == 'transit')
				{
					// ------------------------------------------
					// INBOUND BAGIAN TRANSIT/TRANSFER
					// ------------------------------------------
					$dataArr = $this->statusInboundTransitMau($firstNo);
					$return = ['type' => 2, 'firstData' => $dataArr];
				}
				else
				{
					$return = ['firstData' => 'Not Found !'];
				}
			} 
			else
			{
				$this->db->select('id_, gate_type');
				$this->db->where(['waybill_smu' => $firstNo, '_is_active' => 1]);
				$getHeaderOutbond = $this->db->get('th_outbond');
				$checkHeaderOutbond = $getHeaderOutbond->num_rows();
				$HeaderOutbond = $getHeaderOutbond->row_array();

				if ($checkHeaderOutbond > 0)
				{
					if ($HeaderOutbond['gate_type'] == 'outgoing' || $HeaderOutbond['gate_type'] == 'ekspor')
					{
						// ------------------------------------------
						// OUTBOND
						// ------------------------------------------
						$dataArr = $this->statusOutbondMau($firstNo);
						$return = ['type' => 3, 'firstData' => $dataArr];
					}
					else
					{
						$return = ['firstData' => 'Not found !'];
					}
				} 
				else
				{
					$return = ['firstData' => 'Not found !'];
				}
			}

			foreach ($dataTrackingNo as $trackingNo) 
			{
				$dataKey[] = $trackingNo;
			}

			if (count($dataKey) > 10)
			{
				$returnAll = ['s' => 'failMax', 'm' => 'Insert max 10 tracking number !'];
			}
			else
			{
				$returnAll = ['s' => 'success', 'dataKey' => $dataKey, 'firstLoad' => $return];
			}
			
		}
		echo json_encode($returnAll); 
	}

	public function getTrackingFirstLoadRa()
	{
		$trackingNo = $this->input->post('trackingNo');
		$trackingNo = str_replace(array("'", '"', '/', "`", '=', '*', '^', '?'), '', $trackingNo);
		if (empty($trackingNo))
		{
			$returnAll = ['s' => 'empty', 'm' => 'TrackingNo cannot be empty !'];
		}
		else
		{
			$trackingNo = str_replace(' ', '', $trackingNo); 
			$dataTrackingNo = explode(',', $trackingNo);
			$dataTrackingNo = array_filter($dataTrackingNo); // delete is empty array
			// $trackingNo = rtrim("$trackingNo", ',');
			$firstNo = $dataTrackingNo[0];

			$this->db->select('id_, gate_type');
			$this->db->where(['waybill_smu' => $firstNo, '_is_active' => 1]);
			$getHeaderRegulated = $this->db->get('th_regulated');
			$checkHeaderRegulated = $getHeaderRegulated->num_rows();
			$HeaderRegulated = $getHeaderRegulated->row_array();

			if ($checkHeaderRegulated > 0)
			{
				if ($HeaderRegulated['gate_type'] == 'ekspor' || $HeaderRegulated['gate_type'] == 'outgoing')
				{
					// ------------------------------------------
					// RA 
					// ------------------------------------------
					$dataArr = $this->statusRegulated($firstNo);
					$return = ['type' => 1, 'firstData' => $dataArr];
				}
				else
				{
					$return = ['firstData' => 'Not Found !'];
				}
			} 
			else
			{
				$return = ['firstData' => 'Not found !'];
			}

			foreach ($dataTrackingNo as $trackingNo) 
			{
				$dataKey[] = $trackingNo;
			}

			if (count($dataKey) > 10)
			{
				$returnAll = ['s' => 'failMax', 'm' => 'Insert max 10 tracking number !'];
			}
			else
			{
				$returnAll = ['s' => 'success', 'dataKey' => $dataKey, 'firstLoad' => $return];
			}
			
		}
		echo json_encode($returnAll); 
	}

	public function getTrackingStatusMau($trackingNo)
	{
		// $trackingNo = $this->input->post('trackingNo');
		$this->db->select('id_, gate_type');
		$this->db->where(['waybill_smu' => $trackingNo, '_is_active' => 1]);
		$getHeaderInbound = $this->db->get('th_inbound');
		$checkHeaderInbound = $getHeaderInbound->num_rows();
		$HeaderInbound = $getHeaderInbound->row_array();

		if ($checkHeaderInbound > 0)
		{
			if ($HeaderInbound['gate_type'] == 'import' || $HeaderInbound['gate_type'] == 'incoming')
			{
				// ------------------------------------------
				// INBOUND BAGIAN AWAL
				// ------------------------------------------
				$dataArr = $this->statusInboundMau($trackingNo);
				$return = ['s' => 'success', 'type' => 1, 'data' => $dataArr];
			}
			elseif ($HeaderInbound['gate_type'] == 'transit')
			{
				// ------------------------------------------
				// INBOUND BAGIAN TRANSIT/TRANSFER
				// ------------------------------------------
				$dataArr = $this->statusInboundTransitMau($trackingNo);
				$return = ['s' => 'success', 'type' => 2, 'data' => $dataArr];
			}
			else
			{
				$return = ['firstData' => 'Not Found !'];
			}
		} 
		else
		{
			$this->db->select('id_, gate_type');
			$this->db->where(['waybill_smu' => $trackingNo, '_is_active' => 1]);
			$getHeaderOutbond = $this->db->get('th_outbond');
			$checkHeaderOutbond = $getHeaderOutbond->num_rows();
			$HeaderOutbond = $getHeaderOutbond->row_array();

			if ($checkHeaderOutbond > 0)
			{
				if ($HeaderOutbond['gate_type'] == 'outgoing' || $HeaderOutbond['gate_type'] == 'ekspor')
				{
					// ------------------------------------------
					// OUTBOND
					// ------------------------------------------
					$dataArr = $this->statusOutbondMau($trackingNo);
					$return = ['s' => 'success', 'type' => 3, 'data' => $dataArr];
				}
				else
				{
					$return = ['s' => 'fail', 'm' => 'Data not found !'];
				}
			} 
			else
			{
				$return = ['s' => 'fail', 'm' => 'Data not found !!!'];
			}
		}
		###############################################################################################
		echo json_encode($return); 
	}

	public function getTrackingStatusRa($trackingNo)
	{
		// $trackingNo = $this->input->post('trackingNo');
		$this->db->select('id_, gate_type');
		$this->db->where(['waybill_smu' => $trackingNo, '_is_active' => 1]);
		$getHeaderRegulated = $this->db->get('th_regulated');
		$checkHeaderRegulated = $getHeaderRegulated->num_rows();
		$HeaderRegulated = $getHeaderRegulated->row_array();

		if ($checkHeaderRegulated > 0)
		{
			if ($HeaderRegulated['gate_type'] == 'ekspor' || $HeaderRegulated['gate_type'] == 'outgoing')
			{
				// ------------------------------------------
				// RA 
				// ------------------------------------------
				$dataArr = $this->statusRegulated($trackingNo);
				$return = ['s' => 'success', 'type' => 1, 'data' => $dataArr];
			}
			else
			{
				$return = ['s' => 'fail', 'm' => 'Data not found !'];
			}
		} 
		else
		{
			$return = ['s' => 'fail', 'm' => 'Data not found !!!'];
		}
		###############################################################################################
		echo json_encode($return); 
	}
}
