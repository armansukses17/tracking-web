<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Xml_Library{
    // public $wsdl = 'https://tpsonline.beacukai.go.id/tps/service.asmx?WSDL';
    ##################################################################################################################################
    ################################### PLP SECTION ##################################################################################
    function totalRealiasiBongkarMuat($data)
    {
        foreach($data as $dt){
            $xml = '<DOCUMENT>';
            $xml .=     '<REALISASI>';
            $xml .=         '<REF_NUMBER>' . $dt['ref_numb'] . '</REF_NUMBER>';
            $xml .=         '<KD_TPS>' . $dt['kd_tps'] . '</KD_TPS>';
            $xml .=         '<KD_GUDANG>' . $dt['kd_gudang'] . '</KD_GUDANG>';
            $xml .=         '<TGL_TIBA>' . $dt['tgl_tiba'] . '</TGL_TIBA>';
            $xml .=         '<NM_ANGKUT>' . $dt['nm_angkut'] . '</NM_ANGKUT>';
            $xml .=         '<NO_VOY_FLIGHT>' . $dt['no_voy_flight'] . '</NO_VOY_FLIGHT>';
            $xml .=         '<CALL_SIGN>' . $dt['call_sign'] . '</CALL_SIGN>';
            $xml .=         '<NO_BC11>' . $dt['no_bc11'] . '</NO_BC11>';
            $xml .=         '<TGL_BC11>' . $dt['tgl_bc11'] . '</TGL_BC11>';
            $xml .=         '<JML_BONGKAR_CONTAINER>' . $dt['jml_bongkar_container'] . '</JML_BONGKAR_CONTAINER>';
            $xml .=         '<JML_BONGKAR_KEMASAN>' . $dt['jml_bongkar_kemasan'] . '</JML_BONGKAR_KEMASAN>';
            $xml .=         '<JML_MUAT_CONTAINER>' . $dt['jml_muat_container'] . '</JML_MUAT_CONTAINER>';
            $xml .=         '<JML_MUAT_KEMASAN>' . $dt['jml_muat_kemasan'] . '</JML_MUAT_KEMASAN>';
            $xml .=         '<WK_AKTUAL_KEDATANGAN>' . $dt['wk_aktual_kedatangan'] . '</WK_AKTUAL_KEDATANGAN>';
            $xml .=         '<WK_AKTUAL_KEBERANGKATAN>' . $dt['wk_aktual_keberangkatan'] . '</WK_AKTUAL_KEBERANGKATAN>';
            $xml .=         '<FL_BATAL>' . $dt['fl_batal'] . '</FL_BATAL>';
            $xml .=     '</REALISASI>';
            $xml .= '</DOCUMENT>';
        }
        return $xml;
    }
    
    // Service mendapatkan Respon PLP ondemand
    // a.Format Respon XML Untuk Transaksi Container yang diberikan Ke TPS Asal
    function responOnDemandContAsal($data)
    {
        foreach($data as $dt){
            $xml = '<?xml version="1.0"?>';
            $xml .= '<DOCUMENT>';
            $xml .=     '<RESPONPLP>';
            $xml .=         '<HEADER>';
            $xml .=             '<KD_KANTOR>' . $dt['kd_kantor'] . '</KD_KANTOR>';
            $xml .=             '<KD_TPS_ASAL>' . $dt['kd_tps_asal'] . '</KD_TPS_ASAL>';
            $xml .=             '<KD_TPS_TUJUAN>' . $dt['kd_tps_tujuan'] . '</KD_TPS_TUJUAN>';
            $xml .=             '<REF_NUMBER>' . $dt['reff_numb'] . '</REF_NUMBER>';
            $xml .=             '<GUDANG_ASAL>' . $dt['gudang_asal'] . '</GUDANG_ASAL>';
            $xml .=             '<GUDANG_TUJUAN>' . $dt['gudang_tujuan'] . '</GUDANG_TUJUAN>';
            $xml .=             '<NO_PLP>' . $dt['no_plp'] . '</NO_PLP>';
            $xml .=             '<TGL_PLP>' . $dt['tgl_plp'] . '</TGL_PLP>';
            $xml .=             '<ALASAN_REJECT>' . $dt['alasan_reject'] . '</ALASAN_REJECT>';
            $xml .=             '<CALL_SIGN>' . $dt['call_sign'] . '</CALL_SIGN>';
            $xml .=             '<NM_ANGKUT>' . $dt['nm_angkut'] . '</NM_ANGKUT>';
            $xml .=             '<NO_VOY_FLIGHT>' . $dt['no_voy_flight'] . '</NO_VOY_FLIGHT>';
            $xml .=             '<TGL_TIBA>' . $dt['tgl_tiba'] . '</TGL_TIBA>';
            $xml .=             '<NO_BC11>' . $dt['no_bc11'] . '</NO_BC11>';
            $xml .=             '<TGL_BC11>' . $dt['tgl_bc11'] . '</TGL_BC11>';
            $xml .=             '<NO_SURAT>' . $dt['no_surat'] . '</NO_SURAT>';
            $xml .=             '<TGL_SURAT>' . $dt['tgl_surat'] . '</TGL_SURAT>';
            $xml .=         '</HEADER>';
            $xml .=         '<DETIL>';
            $xml .=             '<CONT>';
            $xml .=                 '<NO_CONT>' . $dt['no_cont'] . '</NO_CONT>';
            $xml .=                 '<UK_CONT>' . $dt['uk_cont'] . '</UK_CONT>';
            $xml .=                 '<FL_SETUJU>' . $dt['fl_setuju'] . '</FL_SETUJU>';
            $xml .=                 '<JNS_CONT>' . $dt['jns_cont'] . '</JNS_CONT>';
            $xml .=             '</CONT>';
            $xml .=         '</DETIL>';
            $xml .=     '</RESPONPLP>';
            $xml .= '</DOCUMENT>';
        }
        return $xml;
    }
    // b. Format Respon XML Untuk Transaksi Container yang diberikan Ke TPS Tujuan
    function responOnDemandContTuj($data)
    {
        foreach($data as $dt){
            $xml = '<?xml version="1.0"?>';
            $xml .= '<DOCUMENT>';
            $xml .=     '<RESPONPLP>';
            $xml .=         '<HEADER>';
            $xml .=             '<KD_KANTOR>' . $dt['kd_kantor'] . '</KD_KANTOR>';
            $xml .=             '<KD_TPS_ASAL>' . $dt['kd_tps_asal'] . '</KD_TPS_ASAL>';
            $xml .=             '<KD_TPS_TUJUAN>' . $dt['kd_tps_tujuan'] . '</KD_TPS_TUJUAN>';
            $xml .=             '<REF_NUMBER>' . $dt['reff_numb'] . '</REF_NUMBER>';
            $xml .=             '<GUDANG_ASAL>' . $dt['gudang_asal'] . '</GUDANG_ASAL>';
            $xml .=             '<GUDANG_TUJUAN>' . $dt['gudang_tujuan'] . '</GUDANG_TUJUAN>';
            $xml .=             '<NO_PLP>' . $dt['no_plp'] . '</NO_PLP>';
            $xml .=             '<TGL_PLP>' . $dt['tgl_plp'] . '</TGL_PLP>';
            $xml .=             '<ALASAN_REJECT>' . $dt['alasan_reject'] . '</ALASAN_REJECT>';
            $xml .=             '<CALL_SIGN>' . $dt['call_sign'] . '</CALL_SIGN>';
            $xml .=             '<NM_ANGKUT>' . $dt['nm_angkut'] . '</NM_ANGKUT>';
            $xml .=             '<NO_VOY_FLIGHT>' . $dt['no_voy_flight'] . '</NO_VOY_FLIGHT>';
            $xml .=             '<TGL_TIBA>' . $dt['tgl_tiba'] . '</TGL_TIBA>';
            $xml .=             '<NO_BC11>' . $dt['no_bc11'] . '</NO_BC11>';
            $xml .=             '<TGL_BC11>' . $dt['tgl_bc11'] . '</TGL_BC11>';
            $xml .=             '<NO_SURAT>' . $dt['no_surat'] . '</NO_SURAT>';
            $xml .=             '<TGL_SURAT>' . $dt['tgl_surat'] . '</TGL_SURAT>';
            $xml .=         '</HEADER>';
            $xml .=         '<DETIL>';
            $xml .=             '<CONT>';
            $xml .=                 '<NO_CONT>' . $dt['no_cont'] . '</NO_CONT>';
            $xml .=                 '<UK_CONT>' . $dt['uk_cont'] . '</UK_CONT>';
            $xml .=                 '<JNS_CONT>' . $dt['jns_cont'] . '</JNS_CONT>';
            $xml .=                 '<NO_POS_BC11>' . $dt['no_pos_bc11'] . '</NO_POS_BC11>';
            $xml .=                 '<CONSIGNEE>' . $dt['consignee'] . '</CONSIGNEE>';
            $xml .=                 '<NO_BL_AWB>' . $dt['no_bl_awb'] . '</NO_BL_AWB>';
            $xml .=                 '<TGL_BL_AWB>' . $dt['tgl_bl_awb'] . '</TGL_BL_AWB>';
            $xml .=                 '<FL_SETUJU>' . $dt['fl_setuju'] . '</FL_SETUJU>';
            $xml .=             '</CONT>';
            $xml .=         '</DETIL>';
            $xml .=     '</RESPONPLP>';
            $xml .= '</DOCUMENT>';
        }
        return $xml;
    }
    // c. Format Respon XML Untuk Transaksi Kemasan yang diberikan Ke TPS Asal
    function responOnDemandKemAsal($data)
    {
        foreach($data as $dt){
            $xml = '<?xml version="1.0"?>';
            $xml .= '<DOCUMENT>';
            $xml .=     '<RESPONPLP>';
            $xml .=         '<HEADER>';
            $xml .=             '<KD_KANTOR>' . $dt['kd_kantor'] . '</KD_KANTOR>';
            $xml .=             '<KD_TPS_ASAL>' . $dt['kd_tps_asal'] . '</KD_TPS_ASAL>';
            $xml .=             '<KD_TPS_TUJUAN>' . $dt['kd_tps_tujuan'] . '</KD_TPS_TUJUAN>';
            $xml .=             '<REF_NUMBER>' . $dt['reff_numb'] . '</REF_NUMBER>';
            $xml .=             '<GUDANG_ASAL>' . $dt['gudang_asal'] . '</GUDANG_ASAL>';
            $xml .=             '<GUDANG_TUJUAN>' . $dt['gudang_tujuan'] . '</GUDANG_TUJUAN>';
            $xml .=             '<NO_PLP>' . $dt['no_plp'] . '</NO_PLP>';
            $xml .=             '<TGL_PLP>' . $dt['tgl_plp'] . '</TGL_PLP>';
            $xml .=             '<ALASAN_REJECT>' . $dt['alasan_reject'] . '</ALASAN_REJECT>';
            $xml .=             '<CALL_SIGN>' . $dt['call_sign'] . '</CALL_SIGN>';
            $xml .=             '<NM_ANGKUT>' . $dt['nm_angkut'] . '</NM_ANGKUT>';
            $xml .=             '<NO_VOY_FLIGHT>' . $dt['no_voy_flight'] . '</NO_VOY_FLIGHT>';
            $xml .=             '<TGL_TIBA>' . $dt['tgl_tiba'] . '</TGL_TIBA>';
            $xml .=             '<NO_BC11>' . $dt['no_bc11'] . '</NO_BC11>';
            $xml .=             '<TGL_BC11>' . $dt['tgl_bc11'] . '</TGL_BC11>';
            $xml .=             '<NO_SURAT>' . $dt['no_surat'] . '</NO_SURAT>';
            $xml .=             '<TGL_SURAT>' . $dt['tgl_surat'] . '</TGL_SURAT>';
            $xml .=         '</HEADER>';
            $xml .=         '<DETIL>';
            $xml .=             '<KMS>';
            $xml .=                 '<JNS_KMS>' . $dt['jns_kms'] . '</JNS_KMS>';
            $xml .=                 '<JML_KMS>' . $dt['jml_kms'] . '</JML_KMS>';
            $xml .=                 '<NO_BL_AWB>' . $dt['no_bl_awb'] . '</NO_BL_AWB>';
            $xml .=                 '<TGL_BL_AWB>' . $dt['tgl_bl_awb'] . '</TGL_BL_AWB>';
            $xml .=                 '<FL_SETUJU>' . $dt['fl_setuju'] . '</FL_SETUJU>';
            $xml .=             '</KMS>';
            $xml .=         '</DETIL>';
            $xml .=     '</RESPONPLP>';
            $xml .= '</DOCUMENT>';
        }
        return $xml;
    }
    // c. Format Respon XML Untuk Transaksi Kemasan yang diberikan Ke TPS Tujuan
    function responOnDemandKemTujuan($data)
    {
        foreach($data as $dt){
            $xml = '<?xml version="1.0"?>';
            $xml .= '<DOCUMENT>';
            $xml .=     '<RESPONPLP>';
            $xml .=         '<HEADER>';
            $xml .=             '<KD_KANTOR>' . $dt['kd_kantor'] . '</KD_KANTOR>';
            $xml .=             '<KD_TPS_ASAL>' . $dt['kd_tps_asal'] . '</KD_TPS_ASAL>';
            $xml .=             '<KD_TPS_TUJUAN>' . $dt['kd_tps_tujuan'] . '</KD_TPS_TUJUAN>';
            $xml .=             '<REF_NUMBER>' . $dt['reff_numb'] . '</REF_NUMBER>';
            $xml .=             '<GUDANG_ASAL>' . $dt['gudang_asal'] . '</GUDANG_ASAL>';
            $xml .=             '<GUDANG_TUJUAN>' . $dt['gudang_tujuan'] . '</GUDANG_TUJUAN>';
            $xml .=             '<NO_PLP>' . $dt['no_plp'] . '</NO_PLP>';
            $xml .=             '<TGL_PLP>' . $dt['tgl_plp'] . '</TGL_PLP>';
            $xml .=             '<ALASAN_REJECT>' . $dt['alasan_reject'] . '</ALASAN_REJECT>';
            $xml .=             '<CALL_SIGN>' . $dt['call_sign'] . '</CALL_SIGN>';
            $xml .=             '<NM_ANGKUT>' . $dt['nm_angkut'] . '</NM_ANGKUT>';
            $xml .=             '<NO_VOY_FLIGHT>' . $dt['no_voy_flight'] . '</NO_VOY_FLIGHT>';
            $xml .=             '<TGL_TIBA>' . $dt['tgl_tiba'] . '</TGL_TIBA>';
            $xml .=             '<NO_BC11>' . $dt['no_bc11'] . '</NO_BC11>';
            $xml .=             '<TGL_BC11>' . $dt['tgl_bc11'] . '</TGL_BC11>';
            $xml .=             '<NO_SURAT>' . $dt['no_surat'] . '</NO_SURAT>';
            $xml .=             '<TGL_SURAT>' . $dt['tgl_surat'] . '</TGL_SURAT>';
            $xml .=         '</HEADER>';
            $xml .=         '<DETIL>';
            $xml .=             '<KMS>';
            $xml .=                 '<JNS_KMS>' . $dt['jns_kms'] . '</JNS_KMS>';
            $xml .=                 '<JML_KMS>' . $dt['jml_kms'] . '</JML_KMS>';
            $xml .=                 '<NO_BL_AWB>' . $dt['no_bl_awb'] . '</NO_BL_AWB>';
            $xml .=                 '<TGL_BL_AWB>' . $dt['tgl_bl_awb'] . '</TGL_BL_AWB>';
            $xml .=                 '<FL_SETUJU>' . $dt['fl_setuju'] . '</FL_SETUJU>';
            $xml .=             '</KMS>';
            $xml .=         '</DETIL>';
            $xml .=     '</RESPONPLP>';
            $xml .= '</DOCUMENT>';
        }
        return $xml;
    }
    ##################################################################################################################################
    ################################### END PLP SECTION ##############################################################################

    ##################################################################################################################################
    ################################### AUTO GATE #################################################################################
    public function postGateOut(array $data = [])
    {
        $xml = '<?xml version="1.0"?>';
        $xml .='<DOCUMENT xmlns="cocokms.xsd">';
                foreach ($data as $v) {
        $xml    .=    '<COCOKMS>';
        $xml    .=       '<HEADER>';
        $xml    .=          '<KD_DOK>'.$v['kd_dok'].'</KD_DOK>';
        $xml    .=           '<KD_TPS>'.$v['kd_tps'].'</KD_TPS>';
        $xml    .=           '<NM_ANGKUT>'.$v['nm_angkut'].'</NM_ANGKUT>';
        $xml    .=           '<NO_VOY_FLIGHT>'.$v['no_voy_flight'].'</NO_VOY_FLIGHT>';
        $xml    .=           '<CALL_SIGN>'.$v['call_sign'].'</CALL_SIGN>';
        $xml    .=           '<TGL_TIBA>'.date('Ymd',strtotime($v['tg_tiba'])).'</TGL_TIBA>';
        $xml    .=           '<KD_GUDANG>'.$v['kd_gudang'].'</KD_GUDANG>';
        $xml    .=           '<REF_NUMBER>'.$this->ref_num($v['kd_tps']).'</REF_NUMBER>';
        $xml    .=       '</HEADER>';
        $xml    .=       '<DETIL>';
        $xml    .=           '<KMS>';
        $xml    .=               '<NO_BL_AWB>'.$v['no_bl_awb'].'</NO_BL_AWB>';
        $xml    .=               '<TGL_BL_AWB>'.date('Ymd',strtotime($v['tgl_bl_awb'])).'</TGL_BL_AWB>';
        $xml    .=               '<NO_MASTER_BL_AWB>'.$v['no_master_bl_awb'].'</NO_MASTER_BL_AWB>';
        $xml    .=               '<TGL_MASTER_BL_AWB>'.date('Ymd',strtotime($v['tgl_master_bl_awb'])).'</TGL_MASTER_BL_AWB>';
        $xml    .=               '<ID_CONSIGNEE>'.$v['id_consignee'].'</ID_CONSIGNEE>';
        $xml    .=               '<CONSIGNEE>'.htmlspecialchars($v['consignee']).'</CONSIGNEE>';
        $xml    .=               '<BRUTO>'.$v['bruto'].'</BRUTO>';
        $xml    .=               '<NO_BC11>'.$v['no_bc11'].'</NO_BC11>';
        $xml    .=               '<TGL_BC11>'.date('Ymd',strtotime($v['tgl_bc11'])).'</TGL_BC11>';
        $xml    .=               '<NO_POS_BC11>'.$v['no_pos_bc11'].'</NO_POS_BC11>';
        $xml    .=               '<CONT_ASAL>'.$v['cont_asal'].'</CONT_ASAL>';
        $xml    .=               '<SERI_KEMAS>'.$v['seri_kem'].'</SERI_KEMAS>';
        $xml    .=               '<KD_KEMAS>'.$v['kd_kem'].'</KD_KEMAS>';
        $xml    .=               '<JML_KEMAS>'.$v['jml_kem'].'</JML_KEMAS>';
        $xml    .=               '<KD_TIMBUN>'.$v['kd_timbun'].'</KD_TIMBUN>';
        $xml    .=               '<KD_DOK_INOUT>'.$v['kd_dok_inout'].'</KD_DOK_INOUT>';
        $xml    .=               '<NO_DOK_INOUT>'.$v['no_dok_inout'].'</NO_DOK_INOUT>';
        $xml    .=               '<TGL_DOK_INOUT>'.date('Ymd',strtotime($v['tgl_dok_inout'])).'</TGL_DOK_INOUT>';
        $xml    .=               '<WK_INOUT>'.$v['wk_inout'].'</WK_INOUT>';
        $xml    .=               '<KD_SAR_ANGKUT_INOUT>'.$v['kd_sar_angkut'].'</KD_SAR_ANGKUT_INOUT>';
        $xml    .=               '<NO_POL>'.$v['no_pol'].'</NO_POL>';
        $xml    .=               '<PEL_MUAT>'.$v['pel_muat'].'</PEL_MUAT>';
        $xml    .=               '<PEL_TRANSIT>'.$v['pel_transit'].'</PEL_TRANSIT>';
        $xml    .=               '<PEL_BONGKAR>'.$v['pel_bongkar'].'</PEL_BONGKAR>';
        $xml    .=               '<GUDANG_TUJUAN>'.$v['gudang_tujuan'].'</GUDANG_TUJUAN>';
        $xml    .=               '<KODE_KANTOR>'.$v['kode_kantor'].'</KODE_KANTOR>';
        $xml    .=               '<NO_DAFTAR_PABEAN>'.$v['no_daftar_pabean'].'</NO_DAFTAR_PABEAN>';
        $xml    .=               '<TGL_DAFTAR_PABEAN>'.date('Ymd',strtotime($v['tgl_daftar_pabean'])).'</TGL_DAFTAR_PABEAN>';
        $xml    .=               '<NO_SEGEL_BC></NO_SEGEL_BC>';
        $xml    .=               '<TGL_SEGEL_BC></TGL_SEGEL_BC>';
        $xml    .=               '<NO_IJIN_TPS>'.$this->no_ijin($v['kode_kantor'])['NO_IJIN_TPS'].'</NO_IJIN_TPS>';
        $xml    .=               '<TGL_IJIN_TPS>'.$this->no_ijin($v['kode_kantor'])['TGL_IJIN_TPS'].'</TGL_IJIN_TPS>';
        $xml    .=           '</KMS>';
        $xml    .=       '</DETIL>';
        $xml    .=   '</COCOKMS>';
                }
        $xml    .='</DOCUMENT>';
        return $xml;
    }
    ##################################################################################################################################
    ################################### END AUTO GATE #################################################################################
}
