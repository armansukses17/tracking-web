<?php

function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('email')) {
        redirect('auth');
    } else {
        $role_id = $ci->session->userdata('role_id');
        $menu = $ci->uri->segment(2);
        // print_r($menu);die;
        $queryMenu = $ci->db->get_where('user_menu', ['controller' => $menu])->row_array();
        $menu_id = $queryMenu['id'];
        $userAccess = $ci->db->get_where('user_access_menu', ['role_id' => $role_id, 'menu_id' => $menu_id]);
        if ($userAccess->num_rows() < 1) {
            if($menu == ''){
                redirect('home/dashboard');
            }else{
                redirect('auth/blocked');
            }
        }
    }
}

function check_access($role_id, $menu_id)
{
    $ci = get_instance();
    $result = $ci->db->get_where('user_access_menu', [
        'role_id' => $role_id,
        'menu_id' => $menu_id
    ]);

    if ($result->num_rows() > 0) {
        return "checked='checked'";
    }
}

function konversiBulan($angka)
{
    $bulanInggris = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    $index = $angka - 1;
    return $bulanInggris[$index];
}

function roundUp($berat,$total_volume)
{
    if((float)$berat >= 20.00){
        if ((float)$berat < (float)$total_volume) {
            return ceil((float)$total_volume * (float)1) / (float)1;
        } else {
            return ceil((float)$berat * (float)1) / (float)1;
        }
    }else{
        if ((float)$berat < (float)$total_volume) {
            return ceil((float)$total_volume * (float)2) / (float)2;
        } else {
            return ceil((float)$berat * (float)2) / (float)2;
        }
    }
}

function ambilRate($jenis_barang,$berat,$tujuan)
{
    $ci = get_instance();

    if($berat>=20.00 && $berat <= 44.00){
        $berat = 20.00;
    }else if($berat >= 45.00 && $berat <=70.00){
        $berat = 45.00;
    }else if($berat >=71.00 && $berat <=99.00){
        $berat = 71.00;
    } else if ($berat >= 100.00 && $berat <= 299.00) {
        $berat = 100.00;
    } else if ($berat >= 300.00 && $berat <= 499.00) {
        $berat = 300.00;
    } else if ($berat >= 500.00 && $berat <= 999.00) {
        $berat = 500.00;
    } else if ($berat > 1000.00) {
        $berat = 1000.00;
    }

    if ($jenis_barang == 1) {
        $jenis_barang = 'Documents';
    } else {
        $jenis_barang = 'Non-Documents';
    }
    $where = [
        'rate_berat'=> $berat,
        'rate_categori' => $jenis_barang,
        'lettercode'=> $tujuan
    ];
    $joinTable[0]['table'] = 't_rate';
    $joinTable[0]['relation'] = 't_rate.id = t_rate_cost.id_rate';
    $joinTable[1]['table'] = 't_rate_zone';
    $joinTable[1]['relation'] = 't_rate_zone.id = t_rate_cost.id_rate_zone';
    $joinTable[2]['table'] = 't_rate_zone_country';
    $joinTable[2]['relation'] = 't_rate_cost.id_rate_country = t_rate_zone_country.id';
    $data = $ci->crud->readDataObject('*','t_rate_cost',$where,$joinTable,'','','');
    $harga = $data[0]->cost;
    return $harga;
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " Puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " Milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " Trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function kodeInvoice()
{
    $ci = get_instance();
    $data = $ci->crud->readDataArray('MAX(invoice_number) as last', 't_invoice', [], [], '', '', '');
    $kodeBarang = $data[0]['last'];
    $date_lama =(int)substr($kodeBarang, 8, 4);
    $noUrut = (int)substr($kodeBarang, 14, 4);
    if($date_lama == date('ym')){
        $noUrut++;
    }else{
        $noUrut= 0001;
    }

    $char = "X01/INV/".date('ym')."/";
    $kodeBarang = $char . sprintf("%04s", $noUrut);
    return $kodeBarang;
}

function mawb($mawb)
{
    $head = substr($mawb,0,3);
    $str1 = substr($mawb,3,4);
    $str2 = substr($mawb,7,4);
    $jadi = $head.' - '.$str1.' '.$str2;
    return $jadi;
}

function flight($flight)
{
    $head = substr($flight,0,2);
    $str1 = substr($flight,2,3);
    $jadi = $head.'-'.$str1;
    return $jadi;
}


function testZip($files,$account_no)
{
    // var_dump($files);die;
    $file_path = $_SERVER['DOCUMENT_ROOT']. '/inomos/assets/file';
    // var_dump($file_path);die;
    // $files = array( '8844_JKT.pdf', 'NPWP_201808008.pdf', 'NPWP_201905001.jpeg');
    $zipname = $account_no.'_'.time().'_files.zip';
    $zip = new ZipArchive;
    $zip->open($zipname, ZipArchive::CREATE);
    foreach ($files as $file) {
        $zip->addFile($file_path.'/'.$file,$file);
    }
    $zip->close();

    header('Content-Type: application/zip');
    header('Content-disposition: attachment; filename=' . $zipname);
    header('Content-Length: ' . filesize($zipname));
    readfile($zipname);
}


function toPercent($nilai,$nilai_pembagi)
{
    $hasil = $nilai / $nilai_pembagi *100;
    if($hasil > 75){
        return 100;
    }
    return $hasil - fmod($hasil,0.5);
}

function toPercentDibalik($nilai){
    return 100-$nilai;
}


function progress($nilai)
{
    if($nilai == 100){
        return "success";
    }else{
        return "danger";
    }
    // if($nilai <= 25){
    //     return "danger";
    // }elseif( $nilai >= 25 && $nilai <= 50){
    //     return "warning";
    // }elseif($nilai >= 50 && $nilai <= 75){
    //     return "info";
    // }else{
    //     return "success";
    // }
}

function toInt($string)
{
    $value = str_replace([' ','-'],'',$string);
    return $value;
}

function toStatus($x)
{
    if($x == 1){
        return '<span class="badge badge-success">No ticket created or no problem found!</span>';
    }else{
        return '<span class="badge badge-danger">Ticket Created!</span>';
    }
}

function active($x){
    if($x == 1){
        return 'Active';
    }else{
        return 'Not Active';
    }
}

function activeStatus($x){
    if ($x == 1) {
        return 'success';
    } else {
        return 'danger';
    }
}

function ambilRole()
{
    $ci = get_instance();
    $role = $ci->crud->readDataObject('*','user_role',[],[],'','','');
    foreach($role as $rl){
        $data_role =   '<option value="'.$rl->id.'">'.$rl->role.'</option>';
    }
    echo dump($data_role);die;
    
}

function timeFromExcelToLinux($excel_time)
{
    return date('Y-m-d',($excel_time - 25569) * 86400);
}
